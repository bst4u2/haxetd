package ; #if !flash


import openfl._internal.swf.SWFLite;
import openfl.display.MovieClip;
import openfl.Assets;


class CreepAnimation1 extends MovieClip {
	
	
	
	
	public function new () {
		
		super ();
		
		if (!SWFLite.instances.exists ("lib/layout/layout.dat")) {
			
			SWFLite.instances.set ("lib/layout/layout.dat", SWFLite.unserialize (Assets.getText ("lib/layout/layout.dat")));
			
		}
		
		var swfLite = SWFLite.instances.get ("lib/layout/layout.dat");
		var symbol = swfLite.symbols.get (22);
		
		__fromSymbol (swfLite, cast symbol);
		
	}
	
	
}


#else
@:bind @:native("CreepAnimation1") class CreepAnimation1 extends flash.display.MovieClip {
	
	
	
	
	public function new () {
		
		super ();
		
	}
	
	
}
#end