package ; #if !flash


import openfl._internal.swf.SWFLite;
import openfl.display.MovieClip;
import openfl.Assets;


class Gun2 extends MovieClip {
	
	
	public var gunPoint (default, null):openfl.display.MovieClip;
	
	
	public function new () {
		
		super ();
		
		if (!SWFLite.instances.exists ("lib/layout/layout.dat")) {
			
			SWFLite.instances.set ("lib/layout/layout.dat", SWFLite.unserialize (Assets.getText ("lib/layout/layout.dat")));
			
		}
		
		var swfLite = SWFLite.instances.get ("lib/layout/layout.dat");
		var symbol = swfLite.symbols.get (8);
		
		__fromSymbol (swfLite, cast symbol);
		
	}
	
	
}


#else
@:bind @:native("Gun2") class Gun2 extends flash.display.MovieClip {
	
	
	public var gunPoint (default, null):openfl.display.MovieClip;
	
	
	public function new () {
		
		super ();
		
	}
	
	
}
#end