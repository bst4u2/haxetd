package;
import openfl.display.MovieClip;
import openfl.display.Sprite;

/**
 * ...
 * @author Yurii Tkachenko
 */
class Bullet extends Sprite
{
	var bulletView:MovieClip;
	var currentTarget:Enemy;
	var damage:Int = 20;
	
	public function new(type:Int) 
	{
		super();
		if (type == 0) {
			bulletView = new Bullet1();
		} else if (type == 1) {
			bulletView = new Bullet2();
			
			damage = 100;
		} else if (type == 2) {
			bulletView = new Bullet3();
			damage = 10;
		}
		this.addChild(bulletView);
	}
	
	public function setTarget(enemy:Enemy):Void {
		currentTarget = enemy;
	}
	
	public function update() {
		if (currentTarget!= null) {
			this.rotation = GeomUtils.rotateFromTo(this, currentTarget);
			if (GeomUtils.distance(this, currentTarget) > 5) {
				GeomUtils.moveFromTo(this, currentTarget, 3);	
			} else {
				GameField.i.killBullet(this);
				currentTarget.damage(damage);
			}
		}
	}
	
}