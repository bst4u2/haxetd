package;

import openfl.display.MovieClip;
import openfl.display.Sprite;

/**
 * ...
 * @author Yurii Tkachenko
 */
class Enemy extends Sprite
{
	private var health:Int = 100;
	private var enemyView:MovieClip;

	public function new() 
	{
		super();
		enemyView = new CreepAnimation1();
		addChild(enemyView);
	}
	public function damage(damage:Int):Void {
		health -= damage;
		if (health < 0) {
			GameField.i.killEnemy(this);
			GameField.i.money += 2;
		}
	}
	
	public function update() {
		this.rotation = GeomUtils.rotateFromTo(this, GameField.i.target);
		if (GeomUtils.distance(this, GameField.i.target) > 5) {
			GeomUtils.moveFromTo(this, GameField.i.target, 0.2);	
		} else {
			GameField.i.killEnemy(this);
			GameField.i.lifes -= 1;
			
		}
		
	}
	
}