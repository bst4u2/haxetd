package;

import openfl.display.Sprite;
import openfl.display.MovieClip;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.events.KeyboardEvent;
import openfl.geom.Point;
import openfl.text.TextField;
import openfl.text.TextFormat;


/**
 * ...
 * @author Yurii Tkachenko
 */
class GameField extends Sprite
{
	private var frames:Float = 0;
	private var towers = [];
	private var enemies = [];
	private var bullets = [];
	public var target:Sprite;
	private var currentTowerIndex:Int = 0;
	
	private var clip:MovieClip;	
	private var typeTF:TextField;
	
	public static var i:GameField;
	public var money:Int = 50;
	public var lifes:Int = 10;
	private var enemySpawn:Int = 80;
	
	
	public function new() 
	{
		super();
		i = this;
		target = new Sprite();
		target.x = 250;
		target.y = 250;
		
		clip = new GameFiledSymbol ();
		addChild(clip);
		clip.addEventListener(MouseEvent.CLICK, addTower);
		this.addEventListener(Event.ENTER_FRAME, onEnterFrame);
		clip.addEventListener(MouseEvent.MOUSE_WHEEL, changeTower);
		initTextFeilds();
		
	}
	
	private function initTextFeilds():Void {
		typeTF = new TextField();
		typeTF.x = 600;
		typeTF.width = 1000;
		typeTF.height = 500;
		typeTF.defaultTextFormat = new TextFormat(null, 30);
		addChild(typeTF);
	}
	
	function addTower(e:MouseEvent) 
	{
		if (money >= 10) {
				var tower:Tower = new Tower(currentTowerIndex);
				addChild(tower);
				tower.x = e.stageX;
				tower.y = e.stageY;
				towers.push(tower);
			money -= 10;
		}

	}
	
	function changeTower(e:MouseEvent) 
	{
		
		if (e.altKey) {
			enemySpawn += Math.floor(e.delta / Math.abs(e.delta));
			return;
		}
		
		currentTowerIndex += Math.floor(e.delta / Math.abs(e.delta));
		
		if (currentTowerIndex > 2) {
			currentTowerIndex = 0;
		}
		
		if (currentTowerIndex < 0) {
			currentTowerIndex = 2;
		}
	}
	
	public function addBullet(bullet:Bullet):Void {
	
		bullets.push(bullet);
		addChild(bullet);
	}
	
	
	
	public function getMeTarget(tower:Tower):Enemy {
	
		for (enemy in enemies) {
			var currentEnemy:Enemy = enemy;
			if (GeomUtils.distance(tower, currentEnemy) < tower.radius) {
				return currentEnemy;
			}
		}
		return null;
	}
	public function killEnemy(enemy:Enemy):Void {
		this.removeChild(enemy);
		enemies.remove(enemy);
	}
	
	public function killBullet(bullet:Bullet):Void {
		this.removeChild(bullet);
		bullets.remove(bullet);
	}
	
	function createEnemy() 
	{
		var enemy:Enemy = new Enemy();
		addChild(enemy);
		enemies.push(enemy);
		var pnt:Point = Point.polar(250, Math.random() * 360);
		enemy.x = target.x + pnt.x;
		enemy.y = target.y + pnt.y;
		
	}
	
	
	
	function onEnterFrame(e) 
	{
		if (GameField.i.lifes == 0) {
				typeTF.text = "Pizdec";
				return;
			}
		frames += 1;
		for(tower in towers) {
			tower.update();
		}
		
		for(enemy in enemies) {
			enemy.update();
		}
		
		for(bullet in bullets) {
			bullet.update();
		}
		
		if (frames % enemySpawn == 0) {
			createEnemy();
			if (enemySpawn > 5) {
			enemySpawn -= 1;	
			}
			
		}
		
		var data:String = "Tower Type: " + ["normal", "power", "fast"][currentTowerIndex];
		data += "\nMoney: " + money;
		data += "\nLifes: " + lifes;
		data += "\nHardness: " + enemySpawn;
		
		data += "\n\nКлик по полю - создать пушку\nКолесико мышки - изменить пушку";
		typeTF.text = data;
	}

	
}
