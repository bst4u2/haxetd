package;
import openfl.display.DisplayObject;
import openfl.geom.Point;

/**
 * ...
 * @author Yurii Tkachenko
 */
class GeomUtils
{

	public function new() 
	{
		
	}
	public static function distance(from:DisplayObject, to:DisplayObject):Float {
		var p1:Point = new Point(from.x, from.y);
		var p2:Point = new Point(to.x, to.y);
		return Point.distance(p1,p2);
	}
	
	public static function moveFromTo(from:DisplayObject, to:DisplayObject, speed:Float) {
		var p1:Point = new Point(from.x, from.y);
		var p2:Point = new Point(to.x, to.y);
		var p3:Point = Point.interpolate(p2, p1, speed / Point.distance(p1, p2));
			
		from.x = p3.x;
		from.y = p3.y;
	}
	
	public static function rotateFromTo(from:DisplayObject, to:DisplayObject):Float {

		 return Math.atan2( to.y - from.y, to.x - from.x) *180 / Math.PI;
	}
	
}