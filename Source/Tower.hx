package;

import openfl.display.MovieClip;
import openfl.display.Sprite;

/**
 * ...
 * @author Yurii Tkachenko
 */
class Tower extends Sprite
{
	public var radius:Float = 100;
	var type:Int;
	var place:MovieClip;
	var gun:MovieClip;
	var currentTarget:Enemy;
	var reloadMax:Float = 60;
	var reload:Float = 0;
	

	public function new(type:Int) 
	{
		super();
		this.type = type;
		place = new BackGround();
		addChild(place);
		if (type == 0) {
			gun = new Gun1();
		} else if (type == 1) {
			gun = new Gun2();
			radius = 200;
			reload = 100;
			
		} else if (type == 2) {
			gun = new Gun3();
			reloadMax = 20;
			
		}
		addChild(gun);
		
	}
	
	private function fire(enemy:Enemy):Void {
		
		var bullet:Bullet = new Bullet(this.type);
		bullet.x = this.x;
		bullet.y = this.y;
		GameField.i.addBullet(bullet);
		bullet.setTarget(currentTarget);
		GeomUtils.moveFromTo(bullet, currentTarget, 15);
		
	}
	
	public function update() {
		currentTarget = GameField.i.getMeTarget(this);
		if (currentTarget!= null) {
			gun.rotation = GeomUtils.rotateFromTo(this, currentTarget);
			
			if (reload < reloadMax) {
				reload += 1;
			} else {
				reload = 0;
				fire(currentTarget);
			}
		} 
	}
	
}